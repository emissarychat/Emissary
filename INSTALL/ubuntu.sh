echo "Emissary - Update APT"
sudo apt-get update -y
sudo apt install make -y
if ! [ -x "$(command -v redis-server)" ]; then
    echo "Emissary - Installing Redis..."
    sudo apt-get install redis-server
    echo "maxmemory 128mb" >> /etc/redis/redis.conf
    echo "maxmemory-policy allkeys-lru" >> /etc/redis/redis.conf
    echo "bind-address 127.0.0.1" >> /etc/redis/redis.conf
    sudo systemctl restart redis-server.service
    sudo systemctl enable redis-server.service
else
    echo "Emissary - Redis found..."
fi
if ! [ -x "$(command -v node)" ]; then
    echo "============="
    echo "Emissary - Installing Node.js..."
    wget https://deb.nodesource.com/setup_9.x
    chmod +x setup_9.x
    ./setup_9.x
    sudo apt install nodejs -y
else
    echo "Node.js Found..."
    echo "Version : $(node -v)"
fi
if ! [ -x "$(command -v npm)" ]; then
    sudo apt install npm -y
fi
if ! [ -x "$(command -v mysql)" ]; then
    sudo apt install mariadb-server
    mysql -e "source sql/user.sql"
    mysql -e "source sql/framework.sql"
    mysql -e "source sql/defaultAccount.sql"
fi
echo "============="
echo "Emissary - Install NPM Libraries"
sudo npm i npm -g
sudo npm install --unsafe-perm
sudo npm audit fix --force
echo "============="
echo "Emissary - Install PM2"
sudo npm install pm2 -g
#create conf.json
if [ ! -e "./conf.json" ]; then
    sudo cp conf.sample.json conf.json
fi
echo "============="
echo "Emissary - Start Emissary and set to start on boot?"
echo "(y)es or (N)o"
read startEmissary
if [ "$startEmissary" = "y" ] || [ "$startEmissary" = "y" ]; then
    sudo pm2 start emissary.js
    sudo pm2 start cron.js
    sudo pm2 startup
    sudo pm2 save
    sudo pm2 list
fi
