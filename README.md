# Emissary
The Open Source Live Chat Solution written in Node.js.

# Why make this?

Emissary is made from an early live chat prototype written in PHP called CloudChat. CloudChat was made because the live chat built into our billing platform was not functioning correctly at the time. I, Moe Alam, also needed practice. The alternatives were also lacking in desired structure and practice. I also wanted a self-hosted solution. This was a perfect chance to hone understanding of Websocket. Originally the project started in PHP mainly because that was the only server side language I knew at the time. Now that has changed with Node.js in the picture. I have fully modified the code to not use any PHP. This code is completely written in Node.js, EJS.

# How to Install on Ubuntu

```
git clone https://gitlab.com/emissarychat/Emissary.git -b dev /home/Emissary
cd /home/Emissary
sh INSTALL/ubuntu.sh
```

# Links

Website - http://emissary.chat
Community Chat (Discord) - https://discord.gg/kpt3CVp
