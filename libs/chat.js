module.exports = function(s,config){
    s.chats={};
    s.chats.fM=function(d,e){e=[];
        d.forEach(function(v){
            if(v){e.push(v)}
        });
        return e;
    };
    s.channels={};
    s.channels.ch=function(d){
        if(s.chatSession[d.uid]&&s.chatSession[d.uid][d.bid]&&s.obk(s.chatSession[d.uid][d.bid]).length>0&&(!s.chatChannelVisitors[d.uid]||!s.chatChannelVisitors[d.uid][d.bid]||s.obk(s.chatChannelVisitors[d.uid][d.bid]).length===0)&&(!s.chatChannels[d.uid]||!s.chatChannels[d.uid][d.bid]||s.obk(s.chatChannels[d.uid][d.bid].j).length===0)){
            d.fn=function(){
                if(s.chatChannels[d.uid]){delete(s.chatChannels[d.uid][d.bid])};if(s.chatChannelVisitors[d.uid]){delete(s.chatChannelVisitors[d.uid][d.bid])};if(s.chatSession[d.uid]){delete(s.chatSession[d.uid][d.bid])};
            }
            d.le=s.chats.fM(s.chatSession[d.uid][d.bid]);d.lo=JSON.stringify(d.le);
            if(s.chatSession[d.uid][d.bid].length>30){
                s.sqlQuery('SELECT id FROM Chats WHERE ke=? AND id=? AND start=?',[d.uid,d.bid,s.chatChannels[d.uid][d.bid].s],function(er,r){
                    if(r&&r[0]){
                        s.sqlQuery("UPDATE Chats SET history=? WHERE ke=? AND id=? AND start=?",[d.lo,d.uid,d.bid,s.chatChannels[d.uid][d.bid].s],function(){
                             d.fn();
                        });
                    }else{
                        s.sqlQuery("INSERT INTO Chats (start,history,id,ke,co) VALUES (?,?,?,?,?)",[s.chatChannels[d.uid][d.bid].s,d.lo,d.bid,d.uid,0],function(){
                             d.fn();
                        });
                    }
                });
            }
        }
    }
}
