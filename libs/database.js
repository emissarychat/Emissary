var mysql = require('mysql'),
    redis = require('redis'),
    sql
module.exports = function(s,config){
    //sql config
    if(config.databaseType === undefined){config.databaseType='mysql'}
    //sql/database connection with knex
    s.databaseOptions = {
      client: config.databaseType,
      connection: config.db,
    }
    if(s.databaseOptions.client.indexOf('sqlite')>-1){
        s.databaseOptions.client = 'sqlite3';
        s.databaseOptions.useNullAsDefault = true;
    }
    if(s.databaseOptions.client === 'sqlite3' && s.databaseOptions.connection.filename === undefined){
        s.databaseOptions.connection.filename = s.mainDirectory+"/shinobi.sqlite"
    }
    s.mergeQueryValues = function(query,values){
        if(!values){values=[]}
        var valuesNotFunction = true;
        if(typeof values === 'function'){
            var values = [];
            valuesNotFunction = false;
        }
        if(values&&valuesNotFunction){
            var splitQuery = query.split('?')
            var newQuery = ''
            splitQuery.forEach(function(v,n){
                newQuery += v
                var value = values[n]
                if(value){
                    if(isNaN(value) || value instanceof Date){
                        newQuery += "'"+value+"'"
                    }else{
                        newQuery += value
                    }
                }
            })
        }else{
            newQuery = query
        }
        return newQuery
    }
    s.stringToSqlTime = function(value){
        newValue = new Date(value.replace('T',' '))
        return newValue
    }
    s.sqlQuery = function(query,values,onMoveOn,hideLog){
        if(!values){values=[]}
        if(typeof values === 'function'){
            var onMoveOn = values;
            var values = [];
        }
        if(!onMoveOn){onMoveOn=function(){}}
        var mergedQuery = s.mergeQueryValues(query,values)
        // s.debugLog('s.sqlQuery QUERY',mergedQuery)
        if(!s.databaseEngine || !s.databaseEngine.raw){
            s.connectDatabase()
        }
        return s.databaseEngine
        .raw(query,values)
        .asCallback(function(err,r){
            if(err && !hideLog){
                console.log('s.sqlQuery QUERY ERRORED',query)
                console.log('s.sqlQuery ERROR',err)
            }
            if(onMoveOn && typeof onMoveOn === 'function'){
                switch(s.databaseOptions.client){
                    case'sqlite3':
                        if(!r)r=[]
                    break;
                    default:
                        if(r)r=r[0]
                    break;
                }
                onMoveOn(err,r)
            }
        })
    }
    s.connectDatabase = function(){
        s.databaseEngine = require('knex')(s.databaseOptions)
    }
    //connect redis
    s.redis = function(){
        red=redis.createClient();
        red.on("error",function(d){
            setTimeout(function(){console.log('Error : Redis, Reconnecting');s.redis()},5000)
        })
    }
    s.redis();
    //redis set value
    s.stt=function(x,y){if(!y||y=="null"){return false;};red.set(x,y)};
    //log to redis
    s.log=function(x,xx,xxx,tt,ti){
        if(!xxx){tt="LOG_"+x}else{tt="LOG_"+x+"_"+xxx};ti=s.moment();
        red.get(tt,function(err,rd){rd=s.jp(rd,[]);
            if(rd.length>49){rd.shift()};rd.push({d:xx,t:ti});s.stt(tt,s.js(rd));
        });xx.time=ti;
        if(xxx){s.sendToAdmins({ulog:xx,bid:xxx,uid:x},x)}
    }
}
