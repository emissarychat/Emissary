var http = require('http'),
    https = require('https'),
    fs = require('fs'),
    express = require('express'),
    PeerServer = require('peer').ExpressPeerServer,
    app = express(),
    server = http.createServer(app)
module.exports = function(s,config,lang,io){
    //attach socket.io
    io.attach(server)
    //attach peerjs
    app.use('/peerjs',PeerServer(server));
    //
    server.listen(config.port,config.bindip,function(){
        console.log('Emissary - PORT : '+config.port);
    });
    //SSL options
    if(config.ssl&&config.ssl.key&&config.ssl.cert){
        config.ssl.key=fs.readFileSync(s.checkRelativePath(config.ssl.key),'utf8')
        config.ssl.cert=fs.readFileSync(s.checkRelativePath(config.ssl.cert),'utf8')
        if(config.ssl.port===undefined){
            config.ssl.port=443
        }
        if(config.ssl.bindip===undefined){
            config.ssl.bindip=config.bindip
        }
        if(config.ssl.ca&&config.ssl.ca instanceof Array){
            config.ssl.ca.forEach(function(v,n){
                config.ssl.ca[n]=fs.readFileSync(s.checkRelativePath(v),'utf8')
            })
        }
        var serverHTTPS = https.createServer(config.ssl,app);
        serverHTTPS.listen(config.ssl.port,config.bindip,function(){
            console.log('SSL Emissary - SSL PORT : '+config.ssl.port);
        });
        io.attach(serverHTTPS);
        app.use('/peerjs',PeerServer(serverHTTPS));
    }
    return app
}
