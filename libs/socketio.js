var moment = require('moment')
module.exports = function(s,config,lang,io){
    //Server Sender
    s.tx = function(z,y,x){
        if(x){return x.broadcast.to(y).emit('ret',z)}
            io.to(y).emit('ret',z);
    }
    //forEach Sender
    s.sendToAnArrayOfUsers = function(z,y){
        (y).forEach(function(v){
            io.to(v).emit('ret',z);
        })
    }
    //Admin Sender
    s.sendToAdmins = function(z,x,u){
        if(u){return u.broadcast.to('1AA_A_'+x).emit('ret',z)};io.to('1AA_A_'+x).emit('ret',z);
    }
    //send to both vis and admins
    s.sendToVisitorsAndAdmins = function(z,x,y){
        s.tx(z,x,y);atx(z,x,y)
    }
    // socket.io handler
    // TO DO : split into separate functions and create RESTful counterpart
    io.on('connect', function (cn) {
    cn.on('f',function(d,q){
        try{
        if(d.bid&&d.uid){
    function tx(z){//Connection Sender
        if(!z.uid){z.uid=d.uid};if(!z.bid){z.bid=d.bid};cn.emit('ret',z);
    }
            switch(d.f){
    //            case're'://record screen functions
    //                switch(d.ff){
    //                    case's'://save recording
    //
    //                            d.start=s.visitors[cn.uid][cn.bid].fti;
    //                            s.sqlQuery("SELECT * from Recordings WHERE ke=? AND id=? AND start=?",[cn.uid,cn.bid,d.start],function(er,r,g){
    //                                if(r&&r[0]){
    //                                    s.sqlQuery("UPDATE Recordings SET data=? WHERE ke=? AND id=? AND start=?",[d.d,cn.uid,cn.bid,d.start])
    //                                }else{
    //                                    s.sqlQuery("INSERT INTO Recordings SET ?",{id:cn.bid,ke:cn.uid,start:d.start,data:d.d});
    //                                }
    //                            })
    //                        })
    //                    break;
    //                }
    //            break;
                case'data':
                    d.a={hook:d.data,uri:'cx',uid:cn.uid,time:s.moment()};d.a.ip=cn.ip;
                    if(d.data.id){d.a.hook.id=cn.bid;}
                    atx(d.a,cn.uid);
                    if(d.data.time===1){d.data.time=d.a.time}
                    if(d.data.stor){
                        if(d.data.stor.get){
                            red.get('CVAL_'+cn.uid,function(er,rd){rd=s.jp(rd);
                                tx({stor:{key:d.data.stor.get,value:rd[d.data.stor.get]}});
                            })
                        }
                        if(d.data.stor.put&&d.data.stor.value){
                            switch(d.data.stor.value){
                                case'/time':
                                    d.data.stor.value=d.a.time;
                                break;
                            }
                            red.get('CVAL_'+cn.uid,function(er,rd){rd=s.jp(rd);
                                rd[d.data.stor.put]=d.data.stor.value;
                                s.stt("CVAL_"+cn.uid,s.js(rd));
                            })
                        }
                    }
                    if(d.data.broadcast===1){s.tx(d.a,cn.uid,cn);}
                    if(d.data.log){
                        if(d.data.note){d.data.log.note=d.data.note;}
                        if(!d.data.log.c){d.data.log.c=1}
                        d.data.log.ip=cn.ip,d.data.log.origin='cx';
                        s.log(cn.uid,d.data.log);
                        if(d.a.hook.id){s.log(cn.uid,d.data.log,cn.bid);}
                    }
                break;
                case's'://SEND FUNCTION
                    switch(d.ff){
                        case'r':
                            if(s.registeredOperator[cn.uid]&&s.registeredOperator[cn.uid][cn.id]){
                                s.tx(d,d.uid+'_'+d.bid,cn);
                            }else{
                                s.sendToVisitorsAndAdmins(d,cn.uid+'_'+cn.bid,cn);
                            }
                        break;
                        case 0://on type
                            s.tx(d,cn.uid+'_'+cn.bid,cn);
                        break;
                        case 1://on dynamic type
                            s.tx(d,d.uid+'_'+d.bid,cn);
                        break;
                        case't'://Run tool functions
                            if(s.registeredOperator[cn.uid]&&s.registeredOperator[cn.uid][cn.id]){
                                d.arr={}
                                if(d.form){
                                    if(d.form.scr&&d.form.pgr){
                                    if(d.form.scr.length!==0){
                                        d.arr.scrollTo=d.form.scr
                                    }
                                    if(d.form.pgr.length!==0){
                                        d.arr.goTo=d.form.pgr
                                    }
                                    }
                                    if(d.form.script.length!==0){
                                        d.arr.script=d.form.script
                                    }
                                }
                                if(d.fn){
                                    d.fn.cn=cn.id;
                                    d.arr=d.fn;
                                }
                                s.tx(d.arr,d.vid);
                            }
                        break;
                        case'a'://Admin to Admin Send Function
                            d.m.time=moment().format();
                            s.tx({amsg:d.m},d.s)
                        break;
                        case'b':
                            if(!d.m||d.m.d===''){return}
                            red.get("CHAT_"+d.uid+"_"+d.bid,function(err,rd){rd=s.jp(rd);
                                if(!rd)rd=[];
                                d.m.time=moment().format();d.m.sender=cn.bid,d.m.rid=d.bid;
                                s.tx({msg:d.m},d.uid+'_'+d.bid,cn);
        //                        if(rd.length>29){delete(rd[0])}
                                rd.push(d.m);
                                s.stt("CHAT_"+d.uid+"_"+d.bid,s.js(rd))
                            });
                        break;
                        case 'c'://clear mmessages
                            if(s.registeredOperator[cn.uid]&&s.registeredOperator[cn.uid][cn.id]){
                                red.get("CHAT_"+d.uid+"_"+d.bid,function(err,rd){rd=s.jp(rd);
                                    if(rd){
                                        switch(d.fff){
                                            case 0:
                                                if(d.t){
                                                d.arr=[];
                                                rd.forEach(function(v){
                                                    if(!v||v.d.indexOf(d.t)>-1){return false};
                                                    d.arr.push(v);
                                                });
                                                rd=d.arr;delete(d.arr);
                                                s.tx(d,d.uid+'_'+d.bid,cn);
                                                }
                                            break;
                                            case'r':
                                                rd=[];
                                                s.tx({remove:d.r},d.uid+'_'+d.bid,cn);
                                            break;
                                        }
                                    }
                                    s.stt("CHAT_"+d.uid+"_"+d.bid,s.js(rd))
                                });
                            }
                        break;
                        default://Default Send Function
                            if(cn.uid&&cn.bid){
                                if(!d.m||d.m.d===''){return}
                                if(!d.u){d.u=d.uid}
                                if(!d.s){d.bi=d.bid}else{d.bi=d.s;}
                                red.get("CHAT_"+d.u+"_"+d.bi,function(err,rd){
                                    if(rd==='{}'){rd='[]'};rd=s.jp(rd,[]);if(!rd){if(!d.y&&(!s.visitors[d.u]||!s.visitors[d.u][d.bi])){
    //                                    s.sqlQuery("SELECT history,user FROM Chats where ke=? AND id=? DESC LIMIT 1",[d.u,d.bi],function(er,r,g){
    //                                        r=JSON.parse(r[0].user);
    //                                        if(r.mail&&s.vmail(r.mail)){
    //                                            d.mailOptions = {
    //                                                from: '"CloudChat Away Message" <message@cloudchat.online>',
    //                                                to: r.mail,
    //                                                subject: 'Sorry we missed you.',
    //                                                text: d.m.d,
    //                                                html: d.m.d
    //                                            };
    //
    //                                            transporter.sendMail(d.mailOptions, function(error, info){
    //                                                if(error){
    //                                                    tx({pnote:'wgp',pnotm:0,b:d.bi,u:d.u});
    //                                                }
    //                                            });
    //                                        }
    //                                    })
                                        tx({pnote:'wgp',pnotm:0,b:d.bi,u:d.u});return;
                                    };rd=[]}
                                    d.m.time=s.moment();
                                    d.m.sender=cn.bid,d.m.rid=d.bi;
                                    if(!d.m.uid){d.m.uid=d.uid};
                                    if(s.registeredOperator[cn.uid]&&!s.registeredOperator[cn.uid][cn.id]){//User
                                        atx({msg:d.m,uid:d.uid},d.u)
                                    }else{//Admin
                                        s.tx({msg:d.m},d.u+'_'+d.s,cn);
                                    }
                                    if(d.y&&rd.length>29){rd.shift()}
                                    rd.push(d.m);
                                    s.stt("CHAT_"+d.u+"_"+d.bi,s.js(rd));
                                    if(d.m.file){
                                        red.get('FILES_'+cn.uid,function(er,rd){rd=s.jp(rd);
                                            switch(d.x){
                                                case 1:
                                                    rd[d.file.name]=d.file;
                                                break;
                                                case 0:
                                                    delete(rd[d.file.name]);
                                                break;
                                            }
                                           s.stt("FILES_"+cn.uid,s.js(rd))
                                        })
                                    }
                                });
                            }
                        break;
                    }
                break;
                case'a'://ADMIN STUFF
                    if(s.registeredOperator[cn.uid]&&s.registeredOperator[cn.uid][cn.id]){
                    switch(d.ff){
                        case'l'://Log functions
                            switch(d.fff){
                                case'd':
                                    red.get("LOG_"+d.uid+"_"+d.bid,function(err,rd){rd=s.jp(rd,[]);
                                        if(rd.length>0){d.ar=[];
                                            rd.forEach(function(v){
                                                if(d.ts!==v.t){d.ar.push(v)}
                                            })
                                            s.stt("LOG_"+d.uid+"_"+d.bid,s.js(d.ar))
                                        }
                                    })
                                break;
                            }
                        break;
                        case'sh'://share controller
                            switch(d.fff){
                                case'o':case'r':
                                    if(d.m&&d.m!==''){
                                        s.sqlQuery("SELECT ke from Users where mail=?",[d.m],function(er,r,g){
                                            if(r&&r[0]){
                                                d.uid=r[0].ke
                                    if(d.uid!==cn.uid){
                                        s.sqlQuery("SELECT shto,perm from Users where ke='"+cn.uid+"'",function(er,r,g){
                                            if(r&&r[0]){r=r[0];
                                                try{r.shto=JSON.parse(r.shto)}catch(e){r.shto=[]}
                                                d.i=r.shto.indexOf(d.uid)
                                                switch(d.fff){
                                                    case'o':
                                                        if(d.i===-1){
                                                                r.shto.push(d.uid)
                                        s.sqlQuery("SELECT name,mail from Users where ke='"+d.uid+"'",function(er,rr,g){rr=rr[0];
                                                                tx({pnote:'lg',pnotm:1,name:rr.name,mail:rr.mail,ke:d.uid})
                                        })
                                                                d.lr=s.lr('1AA_A_'+d.uid)
                                                                if((d.lr instanceof Object || d.lr instanceof Array)&&d.lr.length>0){
                                                                    d.lr.forEach(function(v){
                                                                        if(s.registeredOperator[s.onlineOperators[cn.uid][v].u]&&s.registeredOperator[s.onlineOperators[cn.uid][v].u][v]){
                                                                            s.onlineOperators[cn.uid][v]={u:d.uid}
                                                                            s.gs(v).join('1AA_A_'+cn.uid)
                                                                            s.tx({users:s.visitors[cn.uid],pnote:{title:'Lucky you!',type:'success',text:s.registeredOperator[cn.uid][cn.id].n+' Shared their visitors with you.'}},v)
                                                                            s.sendToVisitorsAndAdmins({ao:s.cd(cn.uid),uid:cn.uid},cn.uid)
                                                                        }
                                                                    })
                                                                }else{
                                                                    return;
                                                                }
                                                        }else{
                                                            tx({pnote:'ncm',pnotm:'Already shared with Operator'});return;
                                                        }
                                                    break;
                                                    case'r':
                                                        if(d.i>-1){
                                                            r.shto=r.shto.filter(function(n){return n!=d.uid})
                                                            tx({pnote:'lg',pnotm:0,u:d.uid})
                                                            d.lr=s.lr('1AA_A_'+d.uid)
                                                            if((d.lr instanceof Object || d.lr instanceof Array)&&d.lr.length>0){
                                                                d.lr.forEach(function(v){
                                                                    if(s.registeredOperator[s.onlineOperators[cn.uid][v].u]&&s.registeredOperator[s.onlineOperators[cn.uid][v].u][v]){
                                                                        s.gs(v).leave('1AA_A_'+cn.uid),delete(s.onlineOperators[cn.uid][v]),s.tx({remove:['[uid="'+cn.uid+'"]']},v);
                                                                        s.sendToVisitorsAndAdmins({ao:s.cd(cn.uid),uid:cn.uid},cn.uid);
                                                                    }
                                                                })
                                                            }else{
                                                                return;
                                                            }
                                                        }else{
                                                            tx({pnote:'ncm',pnotm:'You have not shared with this Operator'})
                                                            return;
                                                        }
                                                    break;
                                                }
                                                s.registeredOperator[cn.uid][cn.id].shto=r.shto;
                                                s.sqlQuery("UPDATE Users SET shto=? WHERE ke=?",[JSON.stringify(r.shto),cn.uid],function(){
                                                s.sqlQuery("SELECT shfr from Users where ke='"+d.uid+"'",function(er,rr,g){
                                            if(rr&&rr[0]){rr=rr[0];
                                                try{rr.shfr=JSON.parse(rr.shfr)}catch(e){rr.shfr=[]}
                                                d.i=rr.shfr.indexOf(cn.uid)
                                                switch(d.fff){
                                                    case'o':
                                                        if(d.i===-1){
                                                            rr.shfr.push(cn.uid)
                                                        }
                                                    break;
                                                    case'r':
                                                        if(d.i>-1){
                                                            rr.shfr=rr.shfr.filter(function(n){return n!=cn.uid})
                                                        }
                                                    break;
                                                }
                                                          s.sqlQuery("UPDATE Users SET shfr=? WHERE ke=?",[JSON.stringify(rr.shfr),d.uid])
                                                       }
                                                })
                                                })
                                            }else{
                                                tx({pnote:'onf'})
                                            }
                                        })
                                    }else{
                                        tx({pnote:'css'})
                                    }
                                    }else{
                                        tx({pnote:'onf'})
                                    }
                                        })
                                    }else{
                                        tx({pnote:'onf'})
                                    }
                                break;
                            }
                        break;
                        case'new':
                            if(cn.uid==='2Df5hBE'||cn.uid==='VTv9w3M'){
                                cn.broadcast.emit('ret',{new:1});
                            }
                        break;
                        case'ver':
                            if(cn.uid==='2Df5hBE'||cn.uid==='VTv9w3M'){
                                s.ver=d.ver;cn.broadcast.emit('ret',{ver:s.ver});
                            }
                        break;
                        case'diag'://get all current server info
    tx({active_users:s.visitors[d.uid],active_admins:s.a,online_admins:s.onlineOperators[d.uid],active_convos:s.c,version:s.ver})
                        break;
                        case'ccj'://join operator channel
                            if(d.x===1){
                                if(!s.chatChannelVisitors[d.uid])s.chatChannelVisitors[d.uid]={};
                                if(!s.chatChannelVisitors[d.uid][d.bid])s.chatChannelVisitors[d.uid][d.bid]={};
                                if(!s.chatChannels[d.uid])s.chatChannels[d.uid]={};
                                red.get("CHAT_"+d.uid+"_"+d.bid,function(err,rd){rd=s.jp(rd);
                                    if(!s.chatChannels[d.uid][d.bid])s.chatChannels[d.uid][d.bid]={j:{},s:s.moment()};
                                    if(!rd)rd=[];
                                    if(!s.chatChannels[d.uid][d.bid].j[cn.uid+'_'+cn.bid]){
                                        cn.join(d.uid+'_'+d.bid);
                                        if(s.registeredOperator[cn.uid]&&s.registeredOperator[cn.uid][cn.id]&&!s.registeredOperator[cn.uid][cn.id].chh[d.uid]){s.registeredOperator[cn.uid][cn.id].chh[d.uid]={}}
                                        s.registeredOperator[cn.uid][cn.id].chh[d.uid][d.bid]={}
                                        s.chatChannels[d.uid][d.bid].j[cn.uid+'_'+cn.bid]=s.ad(s.registeredOperator[cn.uid][cn.id],cn.id);
                                        s.tx({chan:s.chatChannels[d.uid][d.bid],uid:d.uid,bid:d.bid},d.uid+'_'+d.bid);
                                        tx({chad:rd,uid:d.uid,bid:d.bid,chxa:s.chatChannelVisitors[d.uid][d.bid]})
                                    }
    //                            s.stt("CHAT_"+d.u+"_"+d.bi,s.js(rd))
                                });
                            }else{
                                if(s.chatChannels[d.uid]&&s.chatChannels[d.uid][d.bid]&&s.chatChannels[d.uid][d.bid].j[cn.uid+'_'+cn.bid]){
                                    cn.leave(d.uid+'_'+d.bid);
                                    delete(s.registeredOperator[cn.uid][cn.id].chh[d.uid][d.bid]);delete(s.chatChannels[d.uid][d.bid].j[cn.uid+'_'+cn.bid]);
                                    s.tx({chan:s.chatChannels[d.uid][d.bid],uid:d.uid,bid:d.bid,chxa:s.chatChannelVisitors[d.uid][d.bid]},d.uid+'_'+d.bid);
                                }
                            }
                        break;
                        case'cj'://join chatroom for user
                            if(s.registeredOperator[cn.uid]&&s.visitors[d.uid]){
                                if(!d.uid){d.uid=cn.uid}
                                //start join
                                d.ret={cj:d.cj,x:d.x,adm:{x:d.x,u:cn.uid,b:cn.bid,n:s.registeredOperator[cn.uid][cn.id].n,peer:s.registeredOperator[cn.uid][cn.id].peer},uid:d.uid,bid:d.bid};
                                if(d.x!==0){
                                //check if free
                                if(s.registeredOperator[cn.uid][cn.id].level===0){
                                    d.q=0;
                                    s.obk(s.registeredOperator[cn.uid][cn.id].joined).forEach(function(v){
                                        d.q=d.q+s.obk(s.registeredOperator[cn.uid][cn.id].joined[v]).length;
                                    });
                                    if(d.q>5){tx({f:'cjx',uid:d.uid,bid:d.bid});return;}
                                }
                                    if(s.visitors[d.uid]&&s.visitors[d.uid][d.bid]){
                                        if(!s.visitors[d.uid][d.bid].pj[cn.uid]){
                                            s.visitors[d.uid][d.bid].pj[cn.uid]={};
                                        }
                                        if(!s.visitors[d.uid][d.bid].pj[cn.uid][cn.bid]){
                                            s.visitors[d.uid][d.bid].pj[cn.uid][cn.bid]={t:[],te:[]};
                                        }
                                        s.visitors[d.uid][d.bid].pj[cn.uid][cn.bid].t.push(s.moment())
                                        if(!s.visitors[d.uid][d.bid].joined[cn.uid]){
                                            s.visitors[d.uid][d.bid].joined[cn.uid]={};
                                        }
                                        s.visitors[d.uid][d.bid].joined[cn.uid][cn.bid]={u:cn.uid,b:cn.bid,n:s.registeredOperator[cn.uid][cn.id].n}
                                        if(!s.registeredOperator[cn.uid][cn.id].joined[d.uid]){
                                            s.registeredOperator[cn.uid][cn.id].joined[d.uid]={};
                                        }
                                        s.registeredOperator[cn.uid][cn.id].joined[d.uid][d.bid]={u:d.uid,n:s.visitors[d.uid][d.bid].name}
                                        if(s.visitors[d.uid][d.bid].cap==0){d.mom=s.moment(),s.visitors[d.uid][d.bid].cap=d.mom;d.ret.cap=d.mom};
                                        d.ret.adm.pp=s.registeredOperator[cn.uid][cn.id].pp;
                                        if(!d.xx){s.tx(d.ret,d.uid+'_'+d.bid);};
                                        atx({uid:d.uid,bid:d.bid,joined:s.visitors[d.uid][d.bid].joined},d.uid);
                                        cn.join(d.uid+'_'+d.bid);
                                        s.tx({uid:d.uid,bid:d.bid,cap:s.visitors[d.uid][d.bid].cap,pj:s.visitors[d.uid][d.bid].pj},d.uid+'_'+d.bid);
                                    }
                                }else{
                                    cn.leave(d.uid+'_'+d.bid)
                                    if(s.registeredOperator[cn.uid]&&s.registeredOperator[cn.uid][cn.id]&&s.registeredOperator[cn.uid][cn.id].joined[d.uid]&&s.registeredOperator[cn.uid][cn.id].joined[d.uid][d.bid]){
                                        delete(s.registeredOperator[cn.uid][cn.id].joined[d.uid][d.bid]);
                                    }
                                    if(s.visitors[d.uid]&&s.visitors[d.uid][d.bid]&&s.visitors[d.uid][d.bid].joined[cn.uid]&&s.visitors[d.uid][d.bid].joined[cn.uid][cn.bid]&&s.visitors[d.uid][d.bid].pj[cn.uid]){
                                        if(s.visitors[d.uid][d.bid].pj[cn.uid][cn.bid]){s.visitors[d.uid][d.bid].pj[cn.uid][cn.bid].te.push(s.moment())};
                                        delete(s.visitors[d.uid][d.bid].joined[cn.uid][cn.bid]);
                                        if(s.obk(s.visitors[d.uid][d.bid].joined[cn.uid]).length===0){
                                            delete(s.visitors[d.uid][d.bid].joined[cn.uid]);
                                        }
                                        if(!d.xx){s.tx(d.ret,d.uid+'_'+d.bid);};
                                        atx({uid:d.uid,bid:d.bid,joined:s.visitors[d.uid][d.bid].joined},d.uid);
                                        s.tx({uid:d.uid,bid:d.bid,cap:s.visitors[d.uid][d.bid].cap,pj:s.visitors[d.uid][d.bid].pj},d.uid+'_'+d.bid);
                                    }
                                }
                            }
                        break;
                        case'b':
                            s.tx(d,d.uid+'_'+d.bid,cn)
                        break;
                        case'g'://get
                            switch(d.fff){
                                case'r':
                                    if(['rates',''].indexOf(d.ws)>-1){
            s.sqlQuery("UPDATE Details SET "+d.ws+"=? WHERE ke=?",[d.s,cn.uid]);
                                    }
                                break;
                                case's'://stats
                                    if(s.registeredOperator[cn.uid]&&s.registeredOperator[cn.uid][cn.id]){
                                    s.sqlQuery("SELECT * from Crumbs where ke=? AND DATE(`start`) > '"+d.d.start_date+"' and DATE(`end`) < '"+d.d.end_date+"'",[cn.uid],function(er,rr,g){if(er){return;}
                                    s.sqlQuery("SELECT end,user,id from Chats where ke=? AND DATE(`start`) > '"+d.d.start_date+"' and DATE(`end`) < '"+d.d.end_date+"'",[cn.uid],function(er,rrr,g){if(er){return;}
                                    s.sqlQuery("SELECT * from Ratings where ke=? AND DATE(time) between '"+d.d.start_date+"' and '"+d.d.end_date+"'",[cn.uid],function(er,rrrr,g){if(er){return;}
                                    s.sqlQuery("SELECT * from Missed where ke=? AND DATE(`start`) > '"+d.d.start_date+"' and DATE(`end`) < '"+d.d.end_date+"'",[cn.uid],function(er,rrrrr,g){if(er){return;}
                                        tx({stats:{crumbs:rr,chats:rrr,rates:rrrr,miss:rrrrr}});
                                    });
                                    });
                                    });
                                    });
                                    }
                                break;
                                case'g':
                                    s.geo(d.ip,d.bid,d.uid)
                                break;
                                case'c'://chat
                                    if(s.registeredOperator[cn.uid]&&s.registeredOperator[cn.uid][cn.id]){
                                        if(d.tbid&&s.visitors[d.uid]&&s.visitors[d.uid][d.tbid]&&s.visitors[d.uid][d.tbid].vid){
                                    tx({bid:d.tbid,vid:s.visitors[d.uid][d.tbid].vid,cid:cn.id})
                                    if(s.visitors[d.uid][d.tbid].brd){
                                        tx({brd:s.visitors[d.uid][d.tbid].brd,bid:d.tbid})
                                    }
                                    d.arr={bid:d.tbid,uid:d.uid};
                                            red.get("CHAT_"+d.uid+"_"+d.tbid,function(err,rd){rd=s.jp(rd);
                            red.get("LOG_"+d.uid+"_"+d.tbid,function(err,rlog){
                                d.fn=function(){
                                    if((!rd)||(rd&&rd.length===0)){
                                        rd=[]
                                        s.sqlQuery("SELECT history from Chats where id=? AND ke=? ORDER BY `start` DESC LIMIT 1",[d.tbid,d.uid],function(er,r,g){
                                            if(r&&r[0]){
                                                d.arr.history=JSON.parse(r[0].history);
                                            }else{
                                                d.arr.history=rd;
                                            }
                                            tx(d.arr)
                                        })
                                    }else{
                                        d.arr.history=rd;
                                        tx(d.arr)
                                    }
                                }
                                rlog=s.jp(rlog,[]);
                                if(rlog.length===0){
                                    s.sqlQuery("SELECT * from Logs where id=? AND ke=? ORDER BY `start` DESC LIMIT 1",[d.tbid,d.uid],function(er,rr,g){
                                        if(er){return;}
                                        if(rr&&rr[0]){
                                            try{rr=JSON.parse(rr[0].history)}catch(e){rr=[]};//rd=r
                                            d.arr.ulogs=rr;
                                        }
                                        d.fn()
                                    })
                                }else{d.arr.ulogs=rlog;d.fn()}
                                        })
                                        })
                                    }
                                    }
                                break;
                            }
                        break;
                        case's':
                            switch(d.fff){
                                case'op':
                                    s.tx({bg:d.s},d.uid);
                                break;
                                case'f'://settings save
                                    if(s.registeredOperator[cn.uid][cn.id]&&d.s){
                                        if(isNaN(parseInt(d.ws))===false){d.ws=parseInt(d.ws)};
                                            switch(d.ws){
                                                case'app':
                                                    d.fn=function(){
                                                        s.obk(d.s).forEach(function(v){
                                                            if(v=='firebase'&&d.s[v]['apiKey']===''){return}
                                                            d.ar[v]=d.s[v];
                                                        })
                                                    }
                                                    if(d.s.firebase){
                                                        s.stt('FB_'+cn.uid,s.js(d.firebase))
                                                    }else{
                                                        red.del('FB_'+cn.uid);
                                                    }
    //                                                if(cn.mas===1){
                                                      s.sqlQuery("SELECT ids FROM Users WHERE ke=?",[cn.uid],function(er,r){
                                                        d.ar=JSON.parse(r[0].ids);d.fn();
                                                        s.sqlQuery("UPDATE Users SET ids=? WHERE ke=?",[JSON.stringify(d.ar),cn.uid],function(er){
                                                            tx({pnote:'ust'});
                                                        })
                                                      })
    //                                                }else{
    //                                                  s.sqlQuery("SELECT subs FROM Details WHERE ke=?",[cn.uid],function(er,r){
    //                                                      r=JSON.parse(r[0].subs),d.ar=r[cn.bid];d.fn();
    //                                                    r[cn.bid]=d.ar;
    //                                                    s.sqlQuery("UPDATE Details SET subs=? WHERE ke=?",[JSON.stringify(r),cn.uid],function(er){
    //                                                        tx({pnote:'ust'});
    //                                                    })
    //                                                  })
    //                                                }
                                                break;
                                                case'api':
                                                    switch(d.ffff){
                                                        case'i':
                                                  s.sqlQuery("INSERT INTO API (ke,code,detail) VALUES (?,?,?)",[cn.uid,d.s,JSON.stringify({ip:s.registeredOperator[cn.uid][cn.id].ip,name:s.registeredOperator[cn.uid][cn.id].n,bid:cn.bid})],function(er,r){
                                                   tx({pnote:'ust'});
                                                  });
                                                        break;
                                                        case'd':
                                                  s.sqlQuery("DELETE FROM API WHERE ke=? AND code=?",[cn.uid,d.s],function(er,r){
                                                   tx({pnote:'ust'});
                                                  });
                                                        break;
                                                        case'u':
                                                          s.sqlQuery("SELECT * FROM API WHERE ke=? AND code=?",[cn.uid,d.s],function(er,r){
                                                              if(r&&r[0]){
                                                                  r=r[0];r.detail=JSON.parse(r.detail);
                                                                  r.detail[d.n]=d.v;
                                                              s.sqlQuery("UPDATE API SET detail=? WHERE ke=? AND code=?",[JSON.stringify(r.detail),cn.uid,d.s],function(){
                                                                  tx({pnote:'ust'});
                                                              });
                                                              }
                                                          });
                                                        break;
                                                    }
                                                break;
                                                case'profile':
                                                  s.sqlQuery("SELECT ids FROM Users WHERE ke=?",[cn.uid],function(er,r){
                                                    if(r&&r[0]){
                                                        d.vv=s.obk(d.s);d.ar=[];d.arr=[];
                                                        d.vv.forEach(function(v){
                                                         switch(v){
                                                             case'name':case'mail':case'login':case'pass':case'ref':
                                                                 if(d.s[v]==''){return false;}
                                                                 if(v === 'pass'){
                                                                     d.s[v] = s.md5(d.s[v])
                                                                 }
                                                                 d.ar.push(v+"=?");d.arr.push(d.s[v]);
                                                             break;
                                                             case'pp':case'banned':case'bannedf':
                                                                 if(!d.ops){d.ops=JSON.parse(r[0].ids);};
                                                                 if(v==='banned'){s.stt('BANNED_'+cn.uid,JSON.stringify([d.s[v],d.s.bannedf]))}
                                                                 d.ops[v]=d.s[v];
                                                             break;
                                                            }
                                                        });
                                                        if(d.ops){
                                                            d.ar.push('ids=?');d.arr.push(JSON.stringify(d.ops));
                                                        }
                                                        d.arr.push(cn.uid);
                                                        s.sqlQuery("UPDATE Users SET "+d.ar.join(',')+" WHERE ke=?",d.arr,function(er){
                                                            tx({pnote:'ust'});
                                                        })
                                                    }
                                                   })
                                                break;
                                                case 0://dynamic Users saver
                                                    if(d.idx){
                                                        if(d.idx!=='name'||d.idx!=='mail'){return false;}
                                                        if(isNaN(parseInt(''+cn.bid))==false){
                                                            s.sqlQuery("UPDATE Users SET "+d.idx+"=? WHERE ke=?",[d.s,cn.uid],function(er){
                                                                tx({pnote:'ust'});
                                                            })
                                                        }
                                                    }
                                                break;
                                                case 5://dynamic Users.ids saver
                                                    if(d.idx){
                                                    s.sqlQuery("SELECT ids FROM Users WHERE ke=?",[cn.uid],function(er,r){
                                                        if(r&&r[0]){
                                                            r=JSON.parse(r[0].ids);r[d.idx]=d.s;
                                                            if(d.idx==='trusted'){s.stt('TRUST_'+cn.uid,d.s)}
                                                            s.sqlQuery("UPDATE Users SET ids=? WHERE ke=?",[JSON.stringify(r),cn.uid],function(er){
                                                            tx({pnote:'ust'});
                                                            })
                                                        }
                                                    })
                                                    }
                                                break;
                                                case 1:case 3:case 4:case 6:case 8://save function for most of the settings pages
                                                    s.sqlQuery("SELECT ids,type FROM Users WHERE ke=?",[cn.uid],function(er,r){
                                                        if(r&&r[0]){r=r[0];
                                                            //check
                                                            switch(d.ws){
                                                                case 1://sub account limit check
                                                                    d.s=JSON.parse(d.s);
                                                                    d.ss=s.obk(d.s);
                                                                    d.sss={}
                                                                    switch(r.type){
                                                                        case 0:
                                                                            d.ss=d.ss.slice(0,2);
                                                                        break;
                                                                        case 1:
                                                                            d.ss=d.ss.slice(0,5);
                                                                        break;
                                                                    }
                                                                    d.ss.forEach(function(v){
                                                                        d.sss[v]=d.s[v];
                                                                    })
                                                                    d.s=JSON.stringify(d.sss);
                                                                break;
                                                            }
                                                            //submit
                                                            s.sqlQuery("SELECT ke FROM Details WHERE ke=?",[cn.uid],function(er,rr){
                                                                if(rr&&rr[0]){
                                                                    switch(d.ws){case 8:d.ws='chans';break;case 1:d.ws='subs';break;case 3:d.ws='resp';break;
                                                                        case 6:case 4:
                                                                            if(d.ws===6){d.ws='rates',d.wss='ry';s.stt('RAT_K_'+cn.uid,d.sc)}else{d.ws='depts',d.wss='dp';s.stt('DEP_K_'+cn.uid,d.sc)}
                                                                                    r=JSON.parse(r.ids);r[d.wss]=d.sc;s[d.wss][cn.uid]=d.sc;
                                                                                    s.sqlQuery("UPDATE Users SET ids=? WHERE ke=?",[JSON.stringify(r),cn.uid],function(er){d.wss={sc:d.sc};d.wss[d.ws]=d.s;
                                                                                        s.tx(d.wss,cn.uid);
                                                                                    })
                                                                        break;
                                                                    }
                                                                    s.sqlQuery("UPDATE Details SET "+d.ws+"=? WHERE ke=?",[d.s,cn.uid]);
                                                                }else{
                                                                    ['resp','depts','subs','rates','chans'].forEach(function(v){
                                                                        if(!d.s[v]){d.s[v]='{}'}
                                                                    });
                                                                    s.sqlQuery("INSERT INTO Details SET ?",d.s);
                                                                }
                                                                tx({pnote:'ust'});
                                                                //send new settings to other accepted superusers
                                                            })
                                                        }
                                                    })
                                                break;
                                                case 2:
                                                    s.sqlQuery("SELECT ids FROM Users WHERE ke=?",[cn.uid],function(er,r){
                                                        if(r&&r[0]){
                                                            r=JSON.parse(r[0].ids);r.sc=d.gid;
                                                            s.stt('STY_K_',r.sc),s.stt('STY_',d.s);
                                                            s.sqlQuery("UPDATE Users SET ops=?,ids=? WHERE ke=?",[d.s,JSON.stringify(r),cn.uid],function(er){
                                                            tx({pnote:'ust'});s.tx({bg:d.s,sc:d.gid},cn.uid);
                                                            })
                                                        }
                                                    })
                                                break;
                                            }
                                       atx({settings:d},cn.uid,cn)
                                    }
                                break;
                                case'o'://online/offline status and emit count of all
                                    if(d.o){
                                    d.fn=function(v){
                                        s.registeredOperator[v][cn.id].status=d.o;
                                        d.ca={at:s.ad(s.registeredOperator[v][cn.id],cn.id),uid:v}
                                        s.sendToVisitorsAndAdmins(d.ca,v);
                                    };d.fn(cn.uid);
                                    }
                                break;
                            }
                        break;
                        case'e':
                            if(d.form&&d.bid&&d.uid&&s.visitors[d.uid]&&s.visitors[d.uid][d.bid]){
                                if(d.form.name){s.visitors[d.uid][d.bid].name=d.form.name}
                                if(d.form.mail){s.visitors[d.uid][d.bid].mail=d.form.mail}
                                s.tx(d,d.uid+'_'+d.bid);
                            }
                        break;
                    }
                    }else{
                            switch(d.ff){
                        case'su':default://init admin
                                s.sqlQuery("SELECT * from Auth where ke=? AND id=? AND auth=?",[d.uid,d.bid,d.auth],function(er,rr,z){
                                    if(er){cn.disconnect();return}
                                    if(rr&&rr[0]){rr=rr[0];
                                        cn.bid=d.bid,cn.uid=d.uid;cn.mas=rr.mas;
                                        s.sqlQuery("SELECT perm,ids,name,type,shto,shfr from Users where ke='"+d.uid+"' ",function(err,r){
                                            if(err){cn.disconnect();return}
                                            if(r&&r[0]){
                                                r=r[0];d.qs=[];
                                                r.shto=JSON.parse(r.shto);
                                                r.shfr=JSON.parse(r.shfr);
                                            r.shto.forEach(function(v){
                                                d.qs.push("ke='"+v+"'");
                                            })
                                            s.sqlQuery("SELECT ke,perm,name,type,mail from Users where "+(d.qs.join(' OR ')),function(er,g){
                                                tx({shto:g})
                                            })
                                                err=function(xx,r){
                                                    try{r.perm=JSON.parse(r.perm)}catch(t){r.perm={online:5}}
                                                    try{r.ids=JSON.parse(r.ids)}catch(t){r.ids={}}
                                                    if(!s.registeredOperator[xx]){s.registeredOperator[xx]={}}
                                                     er=function(){
                                                        s.registeredOperator[xx][cn.id]={ip:cn.request.connection.remoteAddress,uid:xx,vid:cn.id,bid:d.bid,n:r.name,joined:{},pp:r.ids.pp,peer:d.peer,status:1,chh:{}};
                                                        s.registeredOperator[xx][cn.id].level=r.type;
                                                        //s.log(xx,{id:d.bid,ip:cn.ip,status:1});//log
                                                        //admin init with online count check
                                                        z=function(){
                                                            if(!s.onlineOperators[xx]){s.onlineOperators[xx]={}}
                                                            s.onlineOperators[xx][cn.id]={u:cn.uid}
                                                            cn.join('1AA_A_'+xx)
                                                            s.sendToVisitorsAndAdmins({ao:s.cd(xx),uid:xx},xx),tx({f:'i',cid:cn.id,users:s.visitors[xx],uid:xx})
                                                        }
                                                        if(r.type==0){
                                                            if(!r.perm.online){r.perm.online=5;}
                                                            if(s.ca(xx)>r.perm.online){
                                                                if(!r.name){r.name=xx}
                                                                tx({pnote:'nmu',pnotm:r.name,uid:xx,ao:s.cd(xx)})
                                                            }else{
                                                                z();
                                                            }
                                                        }else{z();}
                                                    }
                                                   if(cn.mas!==1){
                                                        s.sqlQuery("SELECT subs from Details where ke='"+xx+"' ",function(errr,rrr){
                                                           if(errr){return} rrr=JSON.parse(rrr[0].subs);r.ids.pp=rrr[d.bid].PP;r.name=rrr[d.bid].Name;er();
                                                        })
                                                    }else{
                                                        er()
                                                    }
                                                }
                                                err(cn.uid,r);
                                                if(r.shfr.length>0){
                                                d.qs=[];
                                                r.shfr.forEach(function(v){
                                                    d.qs.push("ke='"+v+"'");
                                                })
                                        s.sqlQuery("SELECT ke,perm,name,type,mail from Users where "+(d.qs.join(' OR ')),function(er,g){
                                            g.forEach(function(v){
                                                err(v.ke,v);
                                            })
                                        })
                                                }

                                            }else{tx({fraud:{}});cn.disconnect()}
                                        });
                                    }else{tx({fraud:{}});cn.disconnect()}
                                });
                            break;
                        }
                        }
                break;
                case'j'://REGULAR USER STUFF
                    switch(d.ff){
                        case'pr':
                            //make so it not send to user other windows
    //                        s.tx(d,d.uid+'_'+d.bid,cn)
                            s.tx(d,d.cn)
                        break;
                        case'n'://name/mail change by user
                            if(d.form&&cn.bid&&cn.uid&&s.visitors[cn.uid]&&s.visitors[cn.uid][cn.bid]){
                                if(d.form.name){s.visitors[cn.uid][cn.bid].name=d.form.name}
                                if(d.form.mail){s.visitors[cn.uid][cn.bid].mail=d.form.mail}
                                d.f='a';d.ff='e';s.sendToVisitorsAndAdmins(d,cn.uid+'_'+cn.bid,cn);
                            }
                        break;
                        case'ch'://regular channel visitor
                            if(d.fff===0&&cn.bid){s.chatChannelVisitors[cn.uid][cn.$bid][cn.bid].n=d.n;return;}
                            clearTimeout(s.clearUserTimeout[d.bid+'_'+d.uid+'_'+d.$bid]);
                            cn.uid=d.uid,cn.bid=d.bid,cn.$bid=d.$bid;
                            if(!s.chatChannelVisitors[d.uid])s.chatChannelVisitors[d.uid]={};
                            if(!s.chatChannelVisitors[d.uid][d.$bid])s.chatChannelVisitors[d.uid][d.$bid]={};
                            if(!s.chatChannelVisitors[d.uid][d.$bid][d.bid]){s.chatChannelVisitors[d.uid][d.$bid][d.bid]=d.u;}
                            s.chatChannelVisitors[d.uid][d.$bid][d.bid].ip=cn.request.connection.remoteAddress,s.chatChannelVisitors[d.uid][d.$bid][d.bid].vid={};
                            s.chatChannelVisitors[d.uid][d.$bid][d.bid].vid[cn.id]={};
                                        red.get("CHAT_"+d.uid+"_"+d.$bid,function(err,rd){rd=s.jp(rd);
                            if(!rd)rd=[];
                            d.tx={ao:s.cd(d.uid),uid:d.uid,chxa:s.chatChannelVisitors[d.uid][d.$bid]};
                            if(s.chatChannels[d.uid]&&s.chatChannels[d.uid][d.$bid]){d.tx.chan=s.chatChannels[d.uid][d.$bid]};d.tx.history=rd;
                            tx(d.tx);s.tx({chxn:1,$bid:d.$bid,bid:d.bid,uid:d.uid},d.uid+'_'+d.$bid);
                            cn.join(d.uid+'_'+d.$bid);
                                        })
                        break;
                        case'x'://user init
                            cn.ip=cn.request.connection.remoteAddress;
                            if(d.u.push!==1){s.stf(d.uid,{ip:cn.ip,trust:d.trust,sc:d.sc,ry:d.ry,dp:d.dp,cn:cn.disconnect},tx);}
                            cn.join(d.uid+'_'+d.bid),cn.join(d.uid);
                            clearTimeout(s.clearUserTimeout[d.bid+'_'+d.uid]);delete(s.clearUserTimeout[d.bid+'_'+d.uid]);
                            cn.uid=d.uid,cn.bid=d.bid,d.u.bid=d.bid,d.u.uid=d.uid
                            if(!s.visitors[d.uid]){s.visitors[d.uid]={}}
                            if(!s.visitors[d.uid][d.bid]){
                                d.u.ft=1,d.u.joined={},s.visitors[d.uid][d.bid]=d.u;
                            }else{++s.visitors[d.uid][d.bid].ft}
                            d.r=d.u.referrer
                            if(d.r===null||d.r===undefined){d.r=''}
                            if(!s.visitors[d.uid][d.bid].vid){
                                s.visitors[d.uid][d.bid].vid={}
                            }
                            s.visitors[cn.uid][cn.bid].url=d.u.url;
                            s.visitors[cn.uid][cn.bid].time=moment().format();
                            s.visitors[cn.uid][cn.bid].user_time=d.u.time;
                            s.visitors[cn.uid][cn.bid].title=d.u.title;
                            if(!s.visitors[d.uid][d.bid].geo){
                                s.geo(cn.ip,d.bid,d.uid)
                            }
                            if(!s.visitors[d.uid][d.bid].cap){s.visitors[d.uid][d.bid].cap=0}
                            s.visitors[d.uid][d.bid].pj={};s.visitors[d.uid][d.bid].referrer=d.u.referrer,s.visitors[d.uid][d.bid].ip=cn.ip,s.visitors[d.uid][d.bid].url=d.u.url,s.visitors[d.uid][d.bid].time=d.u.time,s.visitors[d.uid][d.bid].vid[cn.id]={u:d.u.url},s.visitors[d.uid][d.bid].chat=0,s.visitors[d.uid][d.bid].open=0;
                            if(s.visitors[d.uid][d.bid].ll!==2){s.visitors[d.uid][d.bid].ll=0}
                            if(!s.visitors[d.uid][d.bid].brd){s.visitors[d.uid][d.bid].brd=[]}
                            s.visitors[d.uid][d.bid].brd.push({href:d.u.url,referrer:d.r,ft:s.visitors[d.uid][d.bid].ft,time:s.moment()});d.kl=s.visitors[d.uid][d.bid].brd.length;if(d.kl>250){s.visitors[d.uid][d.bid].brd=s.visitors[d.uid][d.bid].brd.splice(0,(d.kl-250))}
                            red.get("FB_"+d.uid,function(err,rd){rd=s.jp(rd);
                            if(rd&&s.obk(rd).length===0){rd={apiKey:"AIzaSyBZ68U1anJHhKLLt30f66BwmB7ED1ipht0",authDomain: "cloudchat-5bb80.firebaseapp.com",storageBucket: "cloudchat-5bb80.appspot.com"}}
                            s.visitors[d.uid][d.bid].fti=s.moment();d.tx={firebase:rd,ao:s.cd(d.uid),uid:d.uid};if(!s.visitors[d.uid][d.bid].fs||s.visitors[d.uid][d.bid].fs=="1"){d.tx.fs=s.moment();s.visitors[d.uid][d.bid].fs=d.tx.fs;};
                            tx(d.tx);
                            atx({f:'v',u:s.visitors[d.uid][d.bid],vid:s.visitors[d.uid][d.bid].vid,bid:d.bid,cid:cn.id,al:0},d.uid)
                            })
                           break;
                        case'b'://chat end
                            if(s.visitors[cn.uid][cn.bid].chat===1){s.log(cn.uid,{c:3,ip:cn.ip},cn.bid);}
                            s.visitors[cn.uid][cn.bid].chat=0
                            atx(d,cn.uid,cn);
                            d.k=s.obk(s.visitors[cn.uid][cn.bid].vid)
                            s.sendToAnArrayOfUsers({f:'x',ff:0},d.k);
                            //save chat because it ended
                            red.get("CHAT_"+cn.uid+"_"+cn.bid,function(err,rd){rd=s.jp(rd);
                            q={rr:s.visitors[cn.uid][cn.bid]};q.user=JSON.stringify({name:q.rr.name,mail:q.rr.mail,ip:cn.ip,pj:q.rr.pj})
                            if(rd&&rd[0]){q.time=rd[0].time}
                            if(q.rr.cap==0&&rd&&rd[0]&&rd.length>1){
                                q.qu="INSERT INTO Missed (start,id,ke,user) VALUES (?,?,?,?)";
                                q.qa=[q.time,cn.bid,cn.uid,q.user];atx({mhistory:rd,uid:cn.uid,bid:cn.bid,d:q.qa},cn.uid);
                                s.sqlQuery(q.qu,q.qa)
                            }
                            q.qu="SELECT * FROM Chats WHERE id=? AND ke=? AND `start`=?";
                            q.qa=[cn.bid,cn.uid,q.time];
                                q.ar=s.js(rd);
                                if(q.ar!=='[]'&&q.ar!=='{}'){
                                    s.sqlQuery(q.qu,q.qa,function(err,r){
                                        if(!q.rr.co){q.rr.co=1};q.co=q.rr.co;
                                        if(r&&r[0]){
                                            q.qu="UPDATE Chats SET history=?,user=? WHERE `start`=? AND id=? AND ke=?";
                                            q.qa=[q.ar,q.user,q.time,cn.bid,cn.uid];
                                        }else{
                                            q.qu="INSERT INTO Chats (start,history,id,ke,co,user) VALUES (?,?,?,?,?,?)";
                                            q.qa=[q.time,q.ar,cn.bid,cn.uid,q.co,q.user];
                                        }
                                        s.sqlQuery(q.qu,q.qa,function(er){})
                                    })
                                }
                                s.stt("CHAT_"+cn.uid+"_"+cn.bid,s.js(rd))
                            })
                        break;
                        case'o':
                            if(d.bid&&s.visitors[d.uid]&&s.visitors[d.uid][d.bid]){s.visitors[d.uid][d.bid].open=d.o
                            s.tx(d,d.uid+'_'+d.bid);
                            atx(d,d.uid);}
                        break;
                        default://chat init
                            if(s.visitors[cn.uid]&&s.visitors[cn.uid][cn.bid]){
                                if(s.visitors[cn.uid][cn.bid].chat!==1){s.log(cn.uid,{c:2,ip:cn.ip},cn.bid);}
                            if(s.obk(s.visitors[cn.uid][cn.bid].joined).length>0){
                                tx({ad:s.visitors[cn.uid][cn.bid].joined})
                            }
                            s.visitors[cn.uid][cn.bid].name=d.u.name;
                            s.visitors[cn.uid][cn.bid].mail=d.u.mail;
                            s.visitors[cn.uid][cn.bid].dept=d.u.dept;
                            if(d.ch){d.chc=0;
                                d.fn=function(g,h){
                                    if(s.registeredOperator[g]){
                                        d.o=s.obk(s.registeredOperator[g]);
                                        d.o.forEach(function(b){
                                            if(s.registeredOperator[g][b].bid===h){
                                                s.tx({poke:{uid:cn.uid,bid:cn.bid}},s.registeredOperator[g][b].vid)
                                            }
                                        })
                                    }
                                }
                                s.obk(d.ch).forEach(function(v){
                                    d.a=s.obk(d.ch)[d.chc];
                                    if(d.ch[d.a] instanceof Array){
                                        v.forEach(function(b){
                                            d.fn(d.a,b)
                                        })
                                    }else{
                                        d.fn(d.a,d.ch[d.a])
                                    }
                                    ++d.chc
                                })
                            }
    clearTimeout(s.clearUserTimeout[cn.bid+'_'+cn.uid]);delete(s.clearUserTimeout[cn.bid+'_'+cn.uid])
                            d.k=s.obk(s.visitors[d.uid][d.bid].vid),s.visitors[d.uid][d.bid].chat=1;
                            d.tt={f:'ii',u:s.visitors[d.uid][d.bid],bid:d.bid,cid:cn.id};
                            atx(d.tt,d.uid);
                            red.get("CHAT_"+d.uid+"_"+d.bid,function(err,rd){rd=s.jp(rd);
                            if(!rd||(rd&&rd.length===0)){
                                rd=[];
                                        s.sqlQuery("SELECT * from Chats where id=? AND ke=? ORDER BY `start` DESC LIMIT 1",[d.bid,d.uid],function(er,r,g){if(er){return;}
                                            if(r&&r[0]){
                                                try{r=JSON.parse(r[0].history)}catch(e){r=[]};//rd=r
                                                s.sendToAnArrayOfUsers({f:'x',ff:1,history:r},d.k)
                                            }else{
                                                s.sendToAnArrayOfUsers({f:'x',ff:1,history:rd},d.k)
                                            };
                                        })
                            }else{
                                s.sendToAnArrayOfUsers({f:'x',ff:1,history:rd},d.k)
                                if(s.visitors[d.uid][d.bid].ft===1){++s.visitors[d.uid][d.bid].co;}
                            }
                            if(s.visitors[d.uid][d.bid].ll==0){d.tt.al=1;s.visitors[d.uid][d.bid].ll=2}
                            })
                            }
                        break;
                    }
                break;
                default:
                    cn.disconnect();
                break;
            }
            }
            }catch(er){
                console.log(er)
            }
        });
        cn.on( 'disconnect', function(d,q) {
            if(cn.id&&cn.uid&&cn.bid){d={};
                if(s.chatChannelVisitors[cn.uid]&&s.chatChannelVisitors[cn.uid][cn.$bid]&&s.chatChannelVisitors[cn.uid][cn.$bid][cn.bid]){
                    delete(s.chatChannelVisitors[cn.uid][cn.$bid][cn.bid].vid[cn.id])
                    if(s.obk(s.chatChannelVisitors[cn.uid][cn.$bid][cn.bid].vid).length===0){
                        s.clearUserTimeout[cn.bid+'_'+cn.uid+'_'+cn.$bid]=setTimeout(function(q){
                            delete(s.chatChannelVisitors[cn.uid][cn.$bid][cn.bid]);
                            s.tx({chxn:0,$bid:cn.$bid,uid:cn.uid,bid:cn.bid},cn.uid+'_'+cn.$bid);
                        },10000);
                    }
                    return false;
                }
                if(s.registeredOperator[cn.uid]&&s.registeredOperator[cn.uid][cn.id]){
                    d.fn=function(xx){
                        s.obk(s.registeredOperator[xx][cn.id].joined).forEach(function(v){
                            s.obk(s.registeredOperator[xx][cn.id].joined[v]).forEach(function(b){
                                if(s.visitors[v]&&s.visitors[v][b]&&s.visitors[v][b].joined[xx]){
                                    if(s.visitors[v][b].pj&&s.visitors[v][b].pj[xx]&&s.visitors[v][b].pj[xx][cn.bid]){s.visitors[v][b].pj[xx][cn.bid].te.push(s.moment())};
                                    delete(s.visitors[v][b].joined[xx][cn.bid]);d.qq=s.visitors[v][b];
                                    s.tx({cj:s.registeredOperator[xx][cn.id].n,x:0,adm:{x:0,u:cn.uid,n:s.registeredOperator[xx][cn.id],peer:s.registeredOperator[xx][cn.id].peer,b:cn.bid},cap:d.qq.cap,pj:d.qq.pj,uid:v,bid:b},v+'_'+b)
                                    atx({joined:d.qq.joined,uid:v,bid:b},v);
                                }
                            })
                        })
                        s.obk(s.registeredOperator[xx][cn.id].chh).forEach(function(v){
                            s.obk(s.registeredOperator[xx][cn.id].chh[v]).forEach(function(b){
                                if(s.chatChannels[v]&&s.chatChannels[v][b]){
                                    delete(s.chatChannels[v][b].j[xx+'_'+cn.bid]);
                                    s.tx({chan:s.chatChannels[v][b],uid:v,bid:b},v+'_'+b);
                                }
                            })
                        });s.registeredOperator[xx][cn.id].status="0";
                        s.sendToVisitorsAndAdmins({at:s.ad(s.registeredOperator[xx][cn.id],cn.id),x:1,uid:cn.uid},xx)
                        delete(s.onlineOperators[xx][cn.id])
                        delete(s.registeredOperator[xx][cn.id])
                        //s.log(xx,{id:cn.bid,ip:cn.ip,status:2});
                    }
                    d.fn(cn.uid);
                    s.sqlQuery("SELECT shfr from Users where ke='"+cn.uid+"'",function(er,r,g){
                        if(er){return;}
                        r=JSON.parse(r[0].shfr);
                        if(r.length>0){
                            r.forEach(function(v){
                                d.fn(v);
                            })
                        }
                    })
                }
                if(s.visitors[cn.uid]&&s.visitors[cn.uid][cn.bid]&&!(s.p[cn.uid]&&s.p[cn.uid][cn.id])){
                    atx({f:'dii',d:cn.bid,uid:cn.uid,cn:cn.id},cn.uid)
                    if(s.visitors[cn.uid][cn.bid].vid[cn.id]){delete(s.visitors[cn.uid][cn.bid].vid[cn.id])}
                    if(s.visitors[cn.uid]&&s.visitors[cn.uid][cn.bid]&&s.visitors[cn.uid][cn.bid].brd&&s.obk(s.visitors[cn.uid][cn.bid].vid).length===0){
                    s.clearUserTimeout[cn.bid+'_'+cn.uid]=setTimeout(function(q){
                        if(s.visitors[cn.uid]&&s.visitors[cn.uid][cn.bid]&&s.visitors[cn.uid][cn.bid].vid){
                        atx({f:'li',bid:cn.bid,vid:s.visitors[cn.uid][cn.bid].vid,uid:cn.uid},cn.uid);
                        red.get("CHAT_"+cn.uid+"_"+cn.bid,function(err,rd){rd=s.jp(rd);
                        red.get("LOG_"+cn.uid+"_"+cn.bid,function(err,rlog){rlog=s.jp(rlog,[]);
                        //Breadcrumbs
                            q={rr:s.visitors[cn.uid][cn.bid]};if(rd){q.ch=rd}
                            q.time=q.rr.brd[0].time,q.qu="SELECT id FROM Crumbs WHERE id=? AND ke=? AND `start`=?",q.qa=[cn.bid,cn.uid,q.time];
                            s.sqlQuery(q.qu,q.qa,function(err,r){
                            q.j=q.rr.brd;if((q.j instanceof Array)===false){q.j=[]};q.j=JSON.stringify(q.j);
                            if(r&&r[0]){
                                q.qu="UPDATE Crumbs SET brd=? WHERE `start`=? AND id=? AND ke=?";
                                q.qa=[q.j,q.time,cn.bid,cn.uid]
                            }else{
                                q.qu="INSERT INTO Crumbs (id,ke,ip,brd,start) VALUES (?,?,?,?,?)";
                                q.qa=[cn.bid,cn.uid,cn.ip,q.j,q.time]
                            }
                            s.sqlQuery(q.qu,q.qa,function(){
                            if(q.ch&&q.ch[0]){q.time=q.ch[0].time}else{q.time=moment().utcOffset('-0800').format();}
                            q.user=JSON.stringify({name:q.rr.name,mail:q.rr.mail,ip:cn.ip,pj:q.rr.pj});
                            if(q.rr.cap==0&&q.rr.chat===1&&rd&&rd[0]&&rd.length>0){
                                q.qu="INSERT INTO Missed (start,id,ke,user) VALUES (?,?,?,?)";
                                q.qa=[q.time,cn.bid,cn.uid,q.user];atx({mhistory:rd,uid:cn.uid,bid:cn.bid,d:q.qa},cn.uid);
                                s.sqlQuery(q.qu,q.qa)
                            }
                            //Chats
                            q.qu="SELECT id FROM Chats WHERE id=? AND ke=? AND `start`=?";
                            q.qa=[cn.bid,cn.uid,q.time];
                                try{q.ar=JSON.stringify(q.ch)}catch(errr){q.ar='[]'}
                                if(q.ar&&q.ar!=='[]'&&q.ar!=='{}'){
                                    s.sqlQuery(q.qu,q.qa,function(err,r){
                                        if(!q.rr.co){q.rr.co=1};q.co=q.rr.co;
                                        if(r&&r[0]){
                                            q.qu="UPDATE Chats SET history=?,user=? WHERE `start`=? AND id=? AND ke=?";
                                            q.qa=[q.ar,q.user,q.time,cn.bid,cn.uid];
                                        }else{
                                            q.qu="INSERT INTO Chats (start,history,id,ke,co,user) VALUES (?,?,?,?,?,?)";
                                            q.qa=[q.time,q.ar,cn.bid,cn.uid,q.co,q.user];
                                        }
                                        s.sqlQuery(q.qu,q.qa,function(){
                                            if(rlog.length>0){
                                        //Logs
                                            q.time=rlog[0].t;
                                            q.qu="SELECT id FROM Logs WHERE id=? AND ke=? AND `start`=?";
                                            q.qa=[cn.bid,cn.uid,q.time];
                                            try{q.ar=JSON.stringify(rlog)}catch(errr){q.ar='[]'}
                                            if(q.ar&&q.ar!=='[]'&&q.ar!=='{}'){
                                                s.sqlQuery(q.qu,q.qa,function(err,r){
                                                    if(r&&r[0]){
                                                        q.qu="UPDATE Logs SET history=? WHERE `start`=? AND id=? AND ke=?";
                                                        q.qa=[q.ar,q.time,cn.bid,cn.uid];
                                                    }else{
                                                        q.qu="INSERT INTO Logs (start,history,id,ke) VALUES (?,?,?,?)";
                                                        q.qa=[q.time,q.ar,cn.bid,cn.uid];
                                                    }
                                                    s.sqlQuery(q.qu,q.qa)
                                                })
                                            }
                                            red.del("LOG_"+cn.uid+"_"+cn.bid)
                                        //end logs
                                            }
                                        })
                                    })
                                }
                                red.del("CHAT_"+cn.uid+"_"+cn.bid)
                                delete(s.visitors[cn.uid][cn.bid]);
                                })
                            })
                        })
                        })
                    }
                    },10000)
                    }
                }
                if(cn.uuid&&s.p[cn.uuid]&&s.p[cn.uuid][cn.id]){
                    delete(s.p[cn.uuid][cn.id])
                    if(s.obk(s.p[cn.uuid]).length===0){s.p[cn.uuid]}
                }
            }
        });
    });
}
