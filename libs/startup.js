module.exports = function(s,config){
    s.connectDatabase()
    s.onApplicationStartExtensions.forEach(function(extension){
        extension(s,config)
    })
    console.log('Emissary is Ready.')
}
