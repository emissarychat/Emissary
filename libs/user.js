var freegeoip = require('node-freegeoip')
module.exports = function(s,config,lang,io){
    s.lr=function(h){try{return s.obk(io.sockets.adapter.rooms[h])}catch(e){return []}};s.gs=function(h){return io.sockets.connected[h]};
    //Count Admins
    s.ca=function(gt){
        if(s.onlineOperators[gt]){return s.obk(s.onlineOperators[gt]).length+1}else{return 1}
    }
    //admin object for client side
    s.ad=function(u,v){
        return {n:u.n,bid:u.bid,ip:u.ip,pp:u.pp,u:u.uid,c:v,peer:u.peer,status:u.status};
    }
    //Get admin details
    s.cd = function(gt,e){
        if(s.onlineOperators[gt]){
            e={e:{}};
            s.obk(s.onlineOperators[gt]).forEach(function(v){
                e.u=s.onlineOperators[gt][v].u;
                if(s.registeredOperator[e.u]&&s.registeredOperator[e.u][v]){
                    e.e[v]=s.ad(s.registeredOperator[e.u][v],v);
                }
            })
            return e.e;
        }
    }
    //get geo data for ip
    s.geo=function(z,y,x){
        if(freegeoip){
            freegeoip.getLocation(z, function(err,loc) {
             if(loc){
                 if(loc.country_code==='IL'){
                     loc.country_code='PS'
                     loc.country_name='Palestine'
                 }
                loc.flag=loc.country_code.toLowerCase()
                s.visitors[x][y].geo=loc
                s.sendToAdmins({geo:loc,bid:y,uid:x},x)
             }
            });
        }
    }
    //visitor checker
    s.stf=function(x,y,z,srun){
        y.default={sc:"d",ry:"d",dp:"d"};
        //check if banned
        srun=function(rd){
            y.t=false;if(!rd){rd=[''];};if(!rd[0]||typeof rd[0]!=='string'){rd[0]='';};
            rd[0].split(',').forEach(function(v){
                if(y.ip==v){y.t=true}
            })
            if(y.t==true){z({banned:1,script:rd[1]});y.cn();}else{
                //not a banned IP
                srun=function(rd){
    //                y.t=false;
    //                rd.split(',').forEach(function(v){
    //                    if(y.trust.indexOf(v.toLowerCase())>-1){y.t=true}
    //                })
    //                if(y.t==false){
    //                    //not trusted domain
    //                    z({trust:0});y.cn();
    //                }else{
                        //trusted domain, do gets
                        red.get("RAT_K_"+x,function(err,key){//rating
                            red.get("DEP_K_"+x,function(err,key2){//department
                                red.get("STY_K_"+x,function(err,key3){//style
                                    if(!key||key!==y.ry||!key2||key2!==y.dp||!key3||key3!==y.sc){//check all
                                        s.sqlQuery('SELECT ids,ops FROM Users WHERE ke=?',[x],function(er,r){
                                        s.sqlQuery('SELECT rates,depts FROM Details WHERE ke=?',[x],function(er,rr){
                                                if(r&&r[0]&&rr&&rr[0]){
                                                    r=r[0];try{r.ids=JSON.parse(r.ids);}catch(er){r.ids=y.default};
                                                    s.stt("RAT_K_"+x,r.ids.ry),s.stt("DEP_K_"+x,r.ids.dp),s.stt("STY_K_"+x,r.ids.sc);
                                                    z({rates:rr[0].rates,sc:r.ids.ry});
                                                    z({depts:rr[0].depts,sc:r.ids.dp});
                                                    z({bg:r.ops,sc:r.ids.sc});
                                                }
                                            });
                                        });
                                    }
                                });
                            });
                        });
    //                }
                }
                red.get("TRUST_"+x,function(err,rd){
                    if(!rd){
                        s.sqlQuery('SELECT ids FROM Users WHERE ke=?',[x],function(er,r){
                            if(r&&r[0]){r=s.jp(r[0].ids).trusted;s.stt("TRUST_"+x,r);srun(r);}
                        });
                    }else{
                        srun(rd)
                    }
                });
                //
            }
        }
        red.get("BANNED_"+x,function(err,rd){
            if(!rd){
                s.sqlQuery('SELECT ids FROM Users WHERE ke=?',[x],function(er,r){
                    if(r&&r[0]){r=s.jp(r[0].ids);er=r.bannedf;r=r.banned;if(!r||r=="null"){r='';};srun([r,er]);}
                    s.stt("BANNED_"+x,JSON.stringify([r,er]));
                });
            }else{
                srun(s.jp(rd))
            }
        });
    }
}
