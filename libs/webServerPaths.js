var fs = require('fs'),
    express = require("express"),
    bodyParser = require("body-parser")
module.exports = function(s,config,lang,io,app){
    app.use(bodyParser.urlencoded({ extended: false }));
    app.use(bodyParser.json());
    app.set('views', s.mainDirectory + '/web/pages');
    app.set('view engine','ejs');
    app.use('/libs',express.static(s.mainDirectory + '/web/libs'));
    //main page
    app.get('/', function (req,res){
        res.render('index');
    });
    //dashboard
    app.get(['/dashboard','/dashboard/:ke'], function (req,res){
        req.proto=req.headers['x-forwarded-proto']||req.protocol;
        res.render('login',{data:req.params,$_GET:req.query,https:(req.proto==='https'),host:req.protocol+'://'+req.hostname,config:config});
    });
    //login and dashboard
    app.post(['/dashboard','/dashboard/:ke'], function (req,res){
        req.ret={ok:false};
        req.proto=req.headers['x-forwarded-proto']||req.protocol;
        function send (x){
            if(x.success===true){
                x.sudo=function(c){
                    switch(c){
                        case'su':
                    if(x.session['Title']==='Superuser'){return true;}
                        break;
                        default:
                    if(x.session['Title']==='Superuser'||x.session['Title']==='Operator'){return true;}
                        break;
                    }
                    return false;
                }
                x.data={};
                x.$user=x.session;
                x.config=config;
                x.host=req.proto+'://'+req.hostname;
                res.render('dashboard',x)
            }else{
                res.render('login',{data:{ke:x.ke},$_GET:req.query,https:(req.proto==='https'),host:req.proto+'://'+req.get('host'),config:config})
            }
        }
        function addtoSession(g,x){
            if(!req.stop){req.stop=1;}else{return}
            x={
                arr:{
                    mas:g.M,
                    id:g.id,
                    ke:g.ke,
                    auth:g.auth,
                    uni:g.ke+g.id
                }
            }
            s.sqlQuery('SELECT * FROM Auth WHERE uni = ?',[x.arr.uni],function(err,r){
                if(r&&r[0]){
                    s.sqlQuery('UPDATE Auth SET auth=? WHERE uni=?',[g.auth,x.arr.uni])
                }else{
                    x.keys=Object.keys(x.arr);
                    x.ques=[];
                    x.vals=[];
                    x.keys.forEach(function(v,n){
                        x.ques.push('?');
                        x.vals.push(x.arr[v]);
                    });
                    s.sqlQuery('INSERT INTO Auth ('+x.keys.join(',')+') VALUES ('+x.ques.join(',')+')',x.vals)
                }
                send(req.ret)
            })
        }
        req.body.user=req.body.user.toLowerCase().trim()
        if (req.body.user==='') {
            req.ret.msg = "Username field was empty.";
        } else if (req.body.pass==='') {
            req.ret.msg = "Password field was empty.";
        } else if (req.body.user!=='' && req.body.pass!=='') {
            req.where='';
            req.vals=[req.body.user,s.md5(req.body.pass)];
            if(req.body.ke&&req.body.ke!==''){
                req.where=' AND ke = ?';
                req.vals.push(req.body.ke);
            }
            s.sqlQuery('SELECT id,ke,mail,login,ref,name,shto,shfr,ops,ids,type FROM Users WHERE login = ? AND pass = ?'+req.where,req.vals,function(err,r){
                if(r&&r[0]){
                    if(r.length>1){
                        req.ret.msg = "Please choose one of your Keys";
                        req.ret.pkg=r;
                        send(req.ret);
                    }else if(r.length===1){//found master
                        r=r[0];
                        s.sqlQuery('SELECT * FROM API WHERE ke = ?',[r.ke],function(err,api){
                            s.sqlQuery('SELECT * FROM Details WHERE ke = ?',[r.ke],function(err,det){
                                req.ret.session={};
                                req.ret.success=true;
                                r.ids=JSON.parse(r.ids,true);
                                if(r.ids.pp){
                                    req.ret.session.pp = r.ids.pp;
                                }
                                req.ret.session.M = 1;
                                req.ret.session.auth = s.gid(24);
                                req.ret.session.api = api;
                                req.ret.session.lv = r.type;
                                req.ret.session.id = r.id;
                                req.ret.session.ke = r.ke;
                                req.ret.session.ref = r.ref;
                                req.ret.session.mail = r.mail;
                                req.ret.session.login = r.login;
                                req.ret.session.name = r.name;
                                req.ret.session.Title='Superuser';
                                req.ret.session.shto = r.shto;
                                req.ret.session.shfr = r.shfr;
                                req.ret.session.ops= JSON.parse(r.ops,true);
                                req.ret.session.det=det;
                                req.ret.session.ids=r.ids;
                                if(!req.ret.session.det){req.ret.session.det='{}';}
                                addtoSession(req.ret.session);
                            })
                        })
                    }else if(r.length===0){//no master
                        if(req.body.ke&&req.body.ke!==''){//key is set
                        s.sqlQuery('SELECT * FROM Details WHERE ke = ? AND subs LIKE ? AND subs LIKE ?',[req.body.ke,'%"Username":"'+req.body.user+'"%','%"Password":"'+req.body.pass+'"%'],function(err,row){
                            if(r&&r[0]){
                                r=r[0];
                                r.subs=JSON.parse(r.subs,true);
                                Object.keys(r.subs).forEach(function(v,n){//sift sub accounts
                                    if(v.Username.toLowerCase()===req.body.user&&v.Password===req.body.pass){
                                        r=v;
                                    }
                                })
                                s.sqlQuery('SELECT * FROM Users WHERE ke = ?',[r.ke],function(err,ops){
                                    ops=ops[0];
                                    req.ret.session={};
                                    req.ret.ret.success=true;
                                    req.ret.session.M = 0;
                                    req.ret.session.lv = ops.type;
                                    req.ret.session.pp = r.PP;
                                    req.ret.session.id = r.Id;
                                    req.ret.session.auth = s.gid(24);
                                    req.ret.session.ke = req.body.ke;
                                    req.ret.session.name = r.Name;
                                    req.ret.session.mail = r.Username;
                                    req.ret.session.Title = r.Title;
                                    req.ret.session.shto = '[]';
                                    req.ret.session.shfr = '[]';
                                    req.ret.session.ops = '{}';
                                    req.ret.session.det = r;

                                    if(!req.ret.session.ids){
                                        req.ret.session.ids={};
                                    }
                                    Object.keys(r).forEach(function(v,n){
                                        if(v.indexOf('slack')>-1){
                                            req.ret.session.ids[v]=r[n];
                                        }
                                    })
                                    if(r.Title==='Superuser'){
                                        s.sqlQuery('SELECT * FROM API WHERE ke = ?',[r.ke],function(err,api){
                                            req.ret.session.ids=JSON.parse(ops.ids,true);
                                            req.ret.session.api = api;
                                            addtoSession(req.ret.session);
                                        })
                                    }else if(r.Title==='Operator'||r.Title==='Superuser'){
                                        req.ret.session.ops = JSON.parse(ops.ops,true);
                                        addtoSession(req.ret.session);
                                    }
                                })
                            }else{
                                req.ret.msg = "Credentials Incorrect.";
                                send(req.ret)
                            }
                        })
                        }else{
                            req.ret.msg = "Password incorrect, or Account does not exist.";
                            send(req.ret)
                        }
                    }
                }else{
                    req.ret.test = req.body;
                    req.ret.msg = "Password incorrect, or Account does not exist.";
                    send(req.ret)
                }
            })
        }
    });
    //get unsecured
    app.all(['/get/:stuff','/get/:stuff/:f','/get/:stuff/:f/:ff','/get/:stuff/:f/:ff/:fff','/get/:stuff/:f/:ff/:fff/:ffff'],function (req,res){
        res.setHeader('Content-Type','application/json');
        req.ret={ok:false}
        switch(req.params.stuff){
            case'translation':
                req.url='https://translate.yandex.net/api/v1.5/tr.json/translate?key=trnsl.1.1.20160311T042953Z.341f2f63f38bdac6.c7e5c01fff7f57160141021ca61b60e36ff4d379&text='+req.body.t+'&lang='+req.body.fr+'-'+req.body.to;
                request({url:req.url,method:'GET',encoding:'utf8'},function(err,data){
                    req.ret.ok=true;
                    req.ret.text=JSON.parse(data.body)['text'][0];
                    res.end(JSON.stringify(req.ret,null,3));
                })
            break;
        }
    })
    //get secured
    app.all(['/:api/:ke/get/:stuff','/:api/:ke/get/:stuff/:f','/:api/:ke/get/:stuff/:f/:ff','/:api/:ke/get/:stuff/:f/:ff/:fff','/:api/:ke/get/:stuff/:f/:ff/:fff/:ffff'],function (req,res){
        res.setHeader('Content-Type','application/json');
        req.ret={ok:false}
        switch(req.params.stuff){
            case'onlineVisitors':
                req.cities={}
                req.countries={}
                req.newArray=Object.values(s.cloneObject(s.visitors[req.params.ke]))
                req.newArray.forEach(function(v){
                    delete(v.ip)
                    //city group
                    if(v.geo){
                        delete(v.geo.ip)
                        if(!req.cities[v.geo.city]){
                            req.cities[v.geo.city]=[]
                        }
                        //country group
                        if(!req.countries[v.geo.country_code]){
                            req.countries[v.geo.country_code]=[]
                        }
                        req.cities[v.geo.city].push(v)
                        req.countries[v.geo.country_code].push(v)
                    }else{
                        if(!req.cities.unknown){
                            req.cities.unknown=[]
                        }
                        //country group
                        if(!req.countries.unknown){
                            req.countries.unknown=[]
                        }
                        req.cities.unknown.push(v)
                        req.countries.unknown.push(v)
                    }
                })
                res.end(s.s({users:req.newArray,cities:req.cities,countries:req.countries,usersCount:req.newArray.length,countriesCount:s.objectCount(req.countries),citiesCount:s.objectCount(req.cities)},null,3))
            break;
            case'missed':
                req.vals=[req.params.ke];
                req.query='';
                if(req.params.ff&&req.params.ff!==''){req.query+='AND start=? ';req.vals.push(decodeURIComponent(req.params.ff));}
                if(req.params.fff&&req.params.fff!==''){req.query+='AND end=? ';req.vals.push(decodeURIComponent(req.params.fff));}
                s.sqlQuery('SELECT * FROM Missed WHERE ke=? '+req.query+'ORDER BY start ASC LIMIT 20',req.vals,function(err,r){
                    req.ret.ok=true;
                    req.ret.missed=r;
                    res.end(JSON.stringify(req.ret,null,3));
                })
            break;
            case'chat':
                if(!req.body.limit){req.body.limit='LIMIT 1';}else{req.body.limit='LIMIT '+req.body.limit}
                if(req.params.ff){
                    req.vals=[req.params.ke,req.params.f,decodeURIComponent(req.params.ff)];
                    s.sqlQuery('SELECT * FROM Chats WHERE ke=? AND id=? AND start >= DATE(?) ORDER BY start DESC '+req.body.limit,req.vals,function(err,r){
                        s.sqlQuery('SELECT * FROM Crumbs WHERE ke=? AND id=? AND start >= DATE(?) ORDER BY start DESC '+req.body.limit,req.vals,function(err,rr){
                            req.ret.ok=true,
                            req.ret.chat=r,
                            req.ret.crumbs=rr;
                            res.end(JSON.stringify(req.ret,null,3));
                        })
                    })
                }else{
                    req.vals=[req.params.ke,req.params.f];
                    if(req.params.ff&&req.params.ff!==''){
                        req.body.limit='AND start=? '+req.body.limit;
                        req.vals.push(decodeURIComponent(req.params.ff));
                    }
                    s.sqlQuery('SELECT * FROM Chats WHERE ke=? AND id=? '+req.body.limit,req.vals,function(err,r){
                        s.sqlQuery('SELECT * FROM Crumbs WHERE ke=? AND id=? '+req.body.limit,req.vals,function(err,rr){
                            req.ret.ok=true,
                            req.ret.chat=r,
                            req.ret.crumbs=rr;
                            res.end(JSON.stringify(req.ret,null,3));
                        })
                    })
                }
            break;
            case'recs':
                if(req.params.ff){
                   req.ret={ok:false,msg:'File not found'}
                   req.file=__dirname+'/../rec/'+req.params.ke+'/'+req.params.ff+'/'+'.json';
                    if(fs.existSync(req.file)){
                        res.end(fs.readFileSync(req.file,'UTF8'));
                    }else{
                        res.end(JSON.stringify(req.ret,null,3));
                    }
                }else{
                    s.sqlQuery('SELECT * FROM Recordings WHERE ke=?',[req.params.ke],function(err,r){
                        if(err){
                            req.ret=err;
                        }else{
                            req.ret=r;
                        }
                        res.end(JSON.stringify(req.ret,null,3));
                    })
                }
            break;
        }
    });
    //contact form
    app.post('/contact/:ke', function (req,res){
        res.header("Access-Control-Allow-Origin",req.headers.origin);
        res.setHeader('Content-Type', 'application/json');
        req.complete=function(data){
            res.end(s.s(data, null, 3))
        }
        s.sqlQuery('SELECT ke,mail FROM Users WHERE ke=?',[req.params.ke],function(err,r){
            if(r&&r[0]){
                r=r[0]
                req.mailOptions = {
                    from: '"Emissary" <no-reply@emissary.chat>',
                    to: r.mail,
                    subject: "New Message - Unmetered.Chat",
                    html: '',
                };
                req.mailOptions.html +='<table cellspacing="0" cellpadding="0" border="0" style="background-color:#3598dc" bgcolor="#3598dc" width="100%"><tbody><tr><td valign="top" align="center">';
                req.mailOptions.html += "<table cellspacing='0' cellpadding='0' border='0' style='border-radius:5px;margin-top:30px;margin-bottom:50px;overflow: hidden;' width='650'>";
                req.mailOptions.html +="<tbody><tr><td style='font-family:&quot;Myriad Pro&quot;,Arial,Helvetica,sans-serif;line-height:1.6;font-size:14px;color:#4a4a4a;background-color:#f6f6f6;border-bottom:1px solid #f0f0f0' align='center' bgcolor='#f6f6f6' height='64' valign='middle'><img src='https://unmetered.chat/logos/title.png'></td></tr><tr>";
                req.mailOptions.html +='<td style="background:#fff;padding:30px;">';
                req.mailOptions.html +="<p>While you were away from <b>Unmetered.Chat</b> you recieved this message.</p>";
                req.mailOptions.html +='<div style="display:block;border-radius:4px;word-break:break-all;word-wrap:break-word;padding:9.5px;margin:0 0 10px;font-size:12px;line-height:1.42857143;background-color:#f5f5f5;border:1px solid #ccc;color:grey" bgcolor="#f5f5f5">';
                req.mailOptions.html +=req.body.msg;
                req.mailOptions.html +='</div><small style="display:block;margin-bottom:10px">'+req.body.name+" - <a href='mailto:"+req.body.mail+"'>"+req.body.mail+'</a></small><br><a href="mailto:'+req.body.mail+'" style="display: inline-block;border-radius:5px;word-break:break-all;word-wrap:break-word;padding:9.5px;margin:0 0 10px;font-size:12px;line-height:1.42857143;text-decoration: none;background-color: #3598DC;border: 1px solid #3598DC;color: white;">Reply</a></td>';
                req.mailOptions.html +="</tr>";
                req.mailOptions.html +="</tbody></table>";
                req.mailOptions.html +="</td></tr>";
                req.mailOptions.html +="</tbody>";
                nodemailer.sendMail(req.mailOptions, (error, info) => {
                    if (error) {
                        req.complete({ok:false,msg:error})
                        console.log(error)
                        return ;
                    }
                    req.complete({ok:true})
                });
            }else{
                req.complete({ok:false,msg:'Key does not exist'})
            }
        })
    });
    //rating operator chat form
    app.post('/rating/:ke', function (req,res){
        res.header("Access-Control-Allow-Origin",req.headers.origin);
        res.setHeader('Content-Type', 'application/json');
        req.complete=function(data){
            res.end(s.s(data, null, 3))
        }
        s.sqlQuery('SELECT ke,mail,ids FROM Users WHERE ke=?',[req.params.ke],function(err,r){
            if(r&&r[0]){
                r=r[0]
                req.ry=JSON.stringify(r['ids'])['ry'];
                req.body.user=JSON.parse(req.body.user)
                req.next=function(){
                    req.mail=JSON.parse(req.body.rate).wylac
                    req.chat=JSON.parse(req.body.chat)
                    if(req.mail!==''&&s.validateEmail(req.mail)&&req.chat.length>0){
                        req.mailOptions = {
                            from: '"Emissary" <no-reply@emissary.chat>',
                            to: req.mail,
                            subject: "Chat History - Unmetered.Chat",
                            html: '',
                        };
                        req.mailOptions.html +='<table cellspacing="0" cellpadding="0" border="0" style="background-color:#3598dc" bgcolor="#3598dc" width="100%"><tbody><tr><td valign="top" align="center">';
                        req.mailOptions.html += "<table cellspacing='0' cellpadding='0' border='0' style='border-radius:5px;margin-top:30px;margin-bottom:30px;overflow: hidden;' width='650'>";
                        req.mailOptions.html +="<thead><tr style='font-weight:bold;color:#fff'><td style='padding:6px'>Time</td><td style='padding:6px'>Name</td><td style='padding:6px'>Message</td></tr></thead><tbody>";
                        req.chat.forEach(function(v){
                            req.mailOptions.html +="<tr style='color:#fff;border-bottom:1px solid #fff'><td style='padding:6px'>"+v.time+"</td><td style='padding:6px'>"+v.name+"</td><td style='padding:6px'>"+v.text+"</td></tr>";
                        })
                        req.mailOptions.html +="</tbody></table>";
                        req.mailOptions.html +="</table>";
                        nodemailer.sendMail(req.mailOptions, (error, info) => {
                            if (error) {
                                req.complete({ok:false,msg:error})
                                console.log(error)
                                return ;
                            }

                        });
                    }
                    delete(req.body.chat);
                    req.body.ke=req.params.ke;
                    req.body.ip=req.headers['cf-connecting-ip']||req.headers["CF-Connecting-IP"]||req.headers["'x-forwarded-for"]||req.connection.remoteAddress;
                    req.body.user=JSON.stringify(req.body.user);
                    req.InsertKeys=Object.keys(req.body)
                    req.questions=[]
                    req.InsertKeys.forEach(function(){
                        req.questions.push('?')
                    })
                    s.sqlQuery('INSERT INTO Ratings ('+req.InsertKeys.join(',')+') VALUES ('+req.questions.join(',')+')',Object.values(req.body))
                    req.complete({ok:true})
                }
                if(req.ry&&req.ry!=='d'){
                    s.sqlQuery('SELECT rates FROM Details WHERE ke=?',[req.params.ke],function(err,rr){
                        if(rr&&rr[0]){
                            rr=rr[0]
                            req.ry=JSON.parse(rr['rates']).shift();
                            req.yr={}
                            req.ry.forEach(function(v,n){
                                delete(v.d);
                                delete(v.fa);
                                req.yr[n]=v;
                            })
                            delete(req.yr['wylac']);
                            req.body.user.rule=req.yr;
                            req.next()
                        }
                    })
                }else{
                    req.next()
                }
            }else{
                req.complete({ok:false,msg:'Key does not exist'})
            }
        })
    });
    //
    app.post(['/mail'], function(req,res,e) {
        res.render('mail');
    })
    //
    app.all(['/api/:id/:type','/api/:id/:type/:var'], function(req,res,e) {
        e.origin = req.headers.origin;
        res.header("Access-Control-Allow-Origin",e.origin);
        res.header('Access-Control-Allow-Headers', 'X-Requested-With, Content-Type');
        s.sqlQuery('SELECT ke,detail FROM API WHERE code=?',[req.params.id],function(err,r){
            if(r&&r[0]){
                r=r[0];r.detail=JSON.parse(r.detail);e={ip:(req.headers['cf-connecting-ip']||req.headers["CF-Connecting-IP"]||req.headers["'x-forwarded-for"]||req.connection.remoteAddress)};
                if(req.body.data){req.body=JSON.parse(req.body.data)}

    //            if(r.detail.origins&&r.detail.origins.length>3){
    //                e.t=0;
    //                r.detail.origins.split(',').forEach(function(v){
    //                    if(e.origin.indexOf(v)>-1){e.t=1;}
    //                });
    //                if(e.t===0){res.end({ok:false,msg:'Unauthorized Domain'});return false;}
    //            }
                switch(req.params.type){
                    case'logs':
                        if(req.params.var){e.st="LOG_"+r.ke+"_"+req.params.var;}else{e.st="LOG_"+r.ke;}
                        red.get(e.st,function(err,rd){
                            rd=s.jp(rd,[]);res.end({ok:true,logs:rd});
                        })
                    break;
                    case'data':
                        try{req.body=JSON.parse(req.body)}catch(err){}
                        if(req.body.data){req.body=req.body.data=JSON.parse(req.body.data)}
                        e.a={hook:req.body,uri:'cx',uid:r.ke,time:s.moment()};e.a.ip=e.ip;
                        if(req.body.id){e.a.hook.id=req.body.id;}
                        s.sendToAdmins(e.a,r.ke);
                        if(req.body.time===1){req.body.time=e.a.time}
                        if(req.body.stor){
                            if(req.body.stor.get){
                                ree.get('CVAL_'+r.ke,function(er,rd){rd=s.jp(rd);
                                    tx({stor:{key:req.body.stor.get,value:rd[req.body.stor.get]}});
                                })
                            }
                            if(req.body.stor.put&&req.body.stor.value){
                                switch(req.body.stor.value){
                                    case'/time':
                                        req.body.stor.value=e.a.time;
                                    break;
                                }
                                ree.get('CVAL_'+r.ke,function(er,rd){rd=s.jp(rd);
                                    rd[req.body.stor.put]=req.body.stor.value;
                                    s.stt("CVAL_"+r.ke,s.js(rd));
                                })
                            }
                        }
                        if(req.body.broadcast===1){s.tx(e.a,r.ke);}
                        if(req.body.log){
                            try{req.body.log=JSON.parse(req.body.log);}catch(err){}
                            if(!req.body.log||(req.body.log instanceof Object)===false){req.body.log={}}
                            if(req.body.note&&req.body.log.text==='/note'){req.body.log.note=1;req.body.log.text=req.body.note;}
                            if(!req.body.log.c){req.body.log.c=1}
                            req.body.log.ip=e.ip,req.body.log.origin='cx';
                            s.log(r.ke,req.body.log);
                            if(e.a.hook.id){s.log(r.ke,req.body.log,e.a.hook.id);}
                        }
                        res.end(e.s);
                    break;
                    case'app':

                    break;
                    default:
                        s.log(r.ke,{c:18,ip:e.ip,origin:e.origin});
                        res.end({ok:false,msg:'Invalid API'});
                    break;
                }

            }else{
                res.end({ok:false,msg:'no key found'});
            }
        })
    });
    //embed
    app.get('/embed/:ke', function (req,res){
        req.proto=req.headers['x-forwarded-proto']||req.protocol;
        res.header("Access-Control-Allow-Origin",req.headers.origin);
        res.render('embed',{data:req.params,$_GET:req.query,https:(req.proto==='https'),host:req.protocol+'://'+req.hostname,config:config});
    });
}
