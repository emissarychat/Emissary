var config = require(s.mainDirectory + '/conf.json')
module.exports = function(process,__dirname){
    if(config.mail){
        var nodemailer = require('nodemailer').createTransport(config.mail);
    }
    if(config.title===undefined){config.title='Emissary'}
    if(config.port===undefined){config.port=80}
    if(config.ip===undefined||config.ip===''||config.ip.indexOf('0.0.0.0')>-1){config.ip='localhost'}else{config.bindip=config.ip};
    if(config.peerJS===undefined){config.peerJS=true}

    return config
}
