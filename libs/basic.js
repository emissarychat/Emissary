var request = require('request'),
    moment = require('moment'),
    fs = require('fs'),
    exec = require('child_process').exec,
    crypto = require('crypto')
module.exports = function(s,config){
    s.cloneObject=function(obj) {
        return JSON.parse(JSON.stringify(obj))
    }
    s.objectCount=function(obj) {
        return Object.values(obj).length
    }
    s.validateEmail=function(email) {
        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(email);
    }
    //check relative path
    s.checkRelativePath=function(x){
        if(x.charAt(0)!=='/'){
            x=__dirname+'/'+x
        }
        return x
    }
    //json parse
    s.jp=function(d,x){if(!d||d==""||d=="null"){if(x){d=x}else{d={}}};try{d=JSON.parse(d)}catch(er){if(x){d=x}else{d={}}};return d;}
    //json string
    s.js=function(d){d=JSON.stringify(d);if(d=="null"){d=null;};return d;}
    //md5 tag
    s.md5=function(x){return crypto.createHash('md5').update(x).digest("hex");}
    //random tag
    s.gid=function(x){
        if(!x){x=10};var t = "";var p = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
        for( var i=0; i < x; i++ )
            t += p.charAt(Math.floor(Math.random() * p.length));
        return t;
    };
    s.isJson=function(x){
        try{JSON.parse(x);if(!x){return false}}catch(er){return false}
        return true;
    }
}
