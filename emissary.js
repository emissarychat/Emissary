//
// Emissary
// Copyright (C) 2016 Moe Alam, moeiscool
//
//
// # Donate
//
// If you like what I am doing here and want me to continue please consider donating :)
// PayPal : paypal@m03.ca
//
var io = new (require('socket.io'))()
//library loader
var loadLib = function(lib){
    return require(__dirname+'/libs/'+lib+'.js')
}
//process handlers
var s = loadLib('process')(process,__dirname)
//configuration loader
var config = loadLib('config')(s)
//language loader
var lang = loadLib('language')(s,config)
//basic functions
loadLib('basic')(s,config)
//database functions : MySQL and Redis
loadLib('extenders')(s,config)
//database functions : MySQL and Redis
loadLib('database')(s,config)
//web server engine
var app = loadLib('webServer')(s,config,lang,io)
//web server routes
loadLib('webServerPaths')(s,config,lang,io,app)
//websocket handlers
loadLib('socketio')(s,config,lang,io)
//user handlers and functions
loadLib('user')(s,config,lang,io)
//chat handlers and functions
loadLib('chat')(s,config,lang,io)
//startup
loadLib('startup')(s,config,lang,io)
